const Database = require('../database');

module.exports = {
    async index(req,res){
        
        let filter = '';
        if(req.params.id) filter = ' AND ID=' + parseInt(req.params.id);

        var db = new Database;
        const resp = await db.query( 'SELECT * FROM categoria WHERE nomefoto is not null and deletado is null ' + filter ).then( rows => rows );

        return res.json(resp);
    },
    async delete(req,res){
        
        var db = new Database;
        const resp = await db.query( 'update categoria set deletado = 1 WHERE ID=' + parseInt(req.params.id) ).then( rows => rows );

        return res.json(resp);
    },
    async add(req,res){

        const nome = req.body.nome.substring(0,200);
        const thumb = req.body.thumb.substring(0,200);

        var db = new Database;
        const resp = await db.query( `INSERT INTO categoria (nome, thumb) VALUES('${nome}','${thumb}')` ).then( rows => rows );

        return res.json(resp);
    },
    async edit(req,res){

        const id = parseInt(req.params.id);
        const nome = req.body.nome.substring(0,200);
        const thumb = req.body.thumb.substring(0,200);

        var db = new Database;
        const resp = await db.query( `UPDATE categoria SET nome='${nome}', thumb='${thumb}' WHERE id=${id}` ).then( rows => rows );

        return res.json(resp);
    },
}