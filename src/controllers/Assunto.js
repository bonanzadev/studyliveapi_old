const Database = require('../database');

module.exports = {
    async index(req,res){
        
        let filter = '';
        if(req.params.id) filter = ' AND item.id=' + parseInt(req.params.id);

        var db = new Database;
        
        const resp = await db.query( `  SELECT ass.* 
                                        FROM assunto ass
                                        join subcategoria sub on (sub.id = ass.id_subcategoria)
                                        WHERE ass.deletado is null 
                                        AND   sub.id_categoria = ${parseInt(req.params.categoria)}
                                        ${filter} order by ass.nome` ).then( rows => rows );

        return res.json(resp);
    },
    async delete(req,res){
        
        var db = new Database;
        const resp = await db.query( 'update assunto set deletado = 1 WHERE ID=' + parseInt(req.params.id) ).then( rows => rows );

        return res.json(resp);
    },
    async add(req,res){

        const nome = req.body.nome.substring(0,200);

        var db = new Database;
        const resp = await db.query( `INSERT INTO assunto (nome) VALUES('${nome}')` ).then( rows => rows );

        return res.json(resp);
    },
    async edit(req,res){

        const id = parseInt(req.params.id);
        const nome = req.body.nome.substring(0,200);

        var db = new Database;
        const resp = await db.query( `UPDATE assunto SET nome='${nome}' WHERE id=${id}` ).then( rows => rows );

        return res.json(resp);
    },
}