-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 05-Jan-2020 às 21:32
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `studylive`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `assunto`
--

CREATE TABLE `assunto` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `deletado` int(11) DEFAULT NULL,
  `id_subcategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `assunto`
--

INSERT INTO `assunto` (`id`, `nome`, `deletado`, `id_subcategoria`) VALUES
(1, 'Equação 1', NULL, 1),
(2, 'Equação 2', NULL, 1),
(3, 'Opaaaa a1', NULL, 2),
(4, 'Opaaaa a2222', NULL, 2),
(5, 'Poligonos', NULL, 3),
(6, 'Triangulo retangulo', NULL, 3),
(7, 'Verbos', NULL, 4),
(8, 'Adverbios', NULL, 4),
(9, 'Colocação pronominal', NULL, 5),
(10, 'Colocação adverbial', NULL, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `id` int(11) NOT NULL,
  `dt_ini` datetime DEFAULT NULL,
  `dt_fim` datetime DEFAULT NULL,
  `id_usuario_aluno` int(11) NOT NULL,
  `id_usuario_professor` int(11) NOT NULL DEFAULT 0,
  `mensagem` varchar(250) DEFAULT NULL,
  `qtdaula` int(11) DEFAULT NULL,
  `deletado` int(11) DEFAULT NULL,
  `dt_cadastro` datetime DEFAULT NULL,
  `channel` varchar(100) DEFAULT NULL,
  `aconteceu` int(11) DEFAULT NULL,
  `avaliacao` int(11) DEFAULT NULL COMMENT 'De 1 a 5 estrelas',
  `avaliacaoMsg` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`id`, `dt_ini`, `dt_fim`, `id_usuario_aluno`, `id_usuario_professor`, `mensagem`, `qtdaula`, `deletado`, `dt_cadastro`, `channel`, `aconteceu`, `avaliacao`, `avaliacaoMsg`) VALUES
(157, '2019-10-18 10:00:00', '2019-10-18 11:00:00', 1, 0, 'ashd lasjhdl khldkj ahlsjdh laknzm,xcn ,.mqowsd qweo asd jkaslçdkj açskdj çaljsd çlasjd açskjd çalksjd ça çaksljd çalksjd çlkajsd  kljaçsdkjaçlksjdç lajs ', 2, NULL, '2019-10-15 00:00:00', NULL, NULL, NULL, NULL),
(158, '2019-10-19 13:00:00', '2019-10-19 15:00:00', 1, 0, 'ashd lasjhdl khldkj ahlsjdh laknzm,xcn ,.mqowsd qweo asd jkaslçdkj açskdj çaljsd çlasjd açskjd çalksjd ça çaksljd çalksjd ça sd asdasd asdasdasdasdasd dasdasd asda sd asdasdasd asdasd lkajsd  kljaçsdkjaçlksjdç lajs ', 4, NULL, '2019-10-15 00:00:00', NULL, NULL, NULL, NULL),
(160, '2019-10-16 07:00:00', '2019-10-16 08:00:00', 1, 2, 'asda s123123  a zxc azsda sdasdasd dasdasd asd ', 2, NULL, NULL, NULL, 1, 4, NULL),
(161, '2019-10-23 09:00:00', '2019-10-23 10:00:00', 1, 2, NULL, 2, NULL, NULL, NULL, 1, 3, 'Nihc'),
(162, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, 'Gshehehehshd', 1, NULL, '2019-12-08 11:21:21', NULL, NULL, NULL, NULL),
(168, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2, 'Hsjsjshs', 1, NULL, '2020-01-02 21:39:50', '1_242', NULL, NULL, NULL),
(169, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2, 'Hsjsjshs', 1, NULL, '2020-01-02 21:41:59', '1_285', NULL, NULL, NULL),
(170, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2, '', 1, NULL, '2020-01-02 22:04:10', '1_397', NULL, NULL, NULL),
(171, '2020-01-23 21:00:00', '2020-01-23 22:00:00', 1, 0, 'Hehehehe hehehehehe hehehehehe', 2, NULL, '2020-01-04 13:25:17', '1_24', NULL, NULL, NULL),
(172, '2020-01-31 18:00:00', '2020-01-31 18:30:00', 1, 0, 'To ferrado ajuda ae', 1, NULL, '2020-01-04 14:37:16', '1_366', NULL, NULL, NULL),
(173, '2020-01-26 07:00:00', '2020-01-26 07:30:00', 1, 0, '', 1, NULL, '2020-01-04 14:49:16', '1_339', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aulaassunto`
--

CREATE TABLE `aulaassunto` (
  `id` int(11) NOT NULL,
  `id_aula` int(11) NOT NULL,
  `id_assunto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aulaassunto`
--

INSERT INTO `aulaassunto` (`id`, `id_aula`, `id_assunto`) VALUES
(1, 161, 2),
(2, 161, 1),
(13, 168, 1),
(14, 168, 2),
(15, 169, 1),
(16, 169, 2),
(17, 170, 1),
(18, 170, 2),
(19, 171, 1),
(20, 171, 2),
(21, 171, 5),
(22, 172, 8),
(23, 172, 10),
(24, 172, 9),
(25, 172, 7),
(26, 173, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `documento` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `bank`
--

INSERT INTO `bank` (`id`, `codigo`, `nome`, `documento`) VALUES
(970, '001', 'BANCO DO BRASIL S/A', NULL),
(971, '002', 'BANCO CENTRAL DO BRASIL', NULL),
(972, '003', 'BANCO DA AMAZONIA S.A', NULL),
(973, '004', 'BANCO DO NORDESTE DO BRASIL S.A', NULL),
(974, '007', 'BANCO NAC DESENV. ECO. SOCIAL S.A', NULL),
(975, '008', 'BANCO MERIDIONAL DO BRASIL', NULL),
(976, '020', 'BANCO DO ESTADO DE ALAGOAS S.A', NULL),
(977, '021', 'BANCO DO ESTADO DO ESPIRITO SANTO S.A', NULL),
(978, '022', 'BANCO DE CREDITO REAL DE MINAS GERAIS SA', NULL),
(979, '024', 'BANCO DO ESTADO DE PERNAMBUCO', NULL),
(980, '025', 'BANCO ALFA S/A', NULL),
(981, '026', 'BANCO DO ESTADO DO ACRE S.A', NULL),
(982, '027', 'BANCO DO ESTADO DE SANTA CATARINA S.A', NULL),
(983, '028', 'BANCO DO ESTADO DA BAHIA S.A', NULL),
(984, '029', 'BANCO DO ESTADO DO RIO DE JANEIRO S.A', NULL),
(985, '030', 'BANCO DO ESTADO DA PARAIBA S.A', NULL),
(986, '031', 'BANCO DO ESTADO DE GOIAS S.A', NULL),
(987, '032', 'BANCO DO ESTADO DO MATO GROSSO S.A.', NULL),
(988, '033', 'BANCO DO ESTADO DE SAO PAULO S.A', NULL),
(989, '034', 'BANCO DO ESADO DO AMAZONAS S.A', NULL),
(990, '035', 'BANCO DO ESTADO DO CEARA S.A', NULL),
(991, '036', 'BANCO DO ESTADO DO MARANHAO S.A', NULL),
(992, '037', 'BANCO DO ESTADO DO PARA S.A', NULL),
(993, '038', 'BANCO DO ESTADO DO PARANA S.A', NULL),
(994, '039', 'BANCO DO ESTADO DO PIAUI S.A', NULL),
(995, '041', 'BANCO DO ESTADO DO RIO GRANDE DO SUL S.A', NULL),
(996, '047', 'BANCO DO ESTADO DE SERGIPE S.A', NULL),
(997, '048', 'BANCO DO ESTADO DE MINAS GERAIS S.A', NULL),
(998, '059', 'BANCO DO ESTADO DE RONDONIA S.A', NULL),
(999, '070', 'BANCO DE BRASILIA S.A', NULL),
(1000, '104', 'CAIXA ECONOMICA FEDERAL', NULL),
(1001, '106', 'BANCO ITABANCO S.A.', NULL),
(1002, '107', 'BANCO BBM S.A', NULL),
(1003, '109', 'BANCO CREDIBANCO S.A', NULL),
(1004, '116', 'BANCO B.N.L DO BRASIL S.A', NULL),
(1005, '148', 'MULTI BANCO S.A', NULL),
(1006, '151', 'CAIXA ECONOMICA DO ESTADO DE SAO PAULO', NULL),
(1007, '153', 'CAIXA ECONOMICA DO ESTADO DO R.G.SUL', NULL),
(1008, '165', 'BANCO NORCHEM S.A', NULL),
(1009, '166', 'BANCO INTER-ATLANTICO S.A', NULL),
(1010, '168', 'BANCO C.C.F. BRASIL S.A', NULL),
(1011, '175', 'CONTINENTAL BANCO S.A', NULL),
(1012, '184', 'BBA - CREDITANSTALT S.A', NULL),
(1013, '199', 'BANCO FINANCIAL PORTUGUES', NULL),
(1014, '200', 'BANCO FRICRISA AXELRUD S.A', NULL),
(1015, '201', 'BANCO AUGUSTA INDUSTRIA E COMERCIAL S.A', NULL),
(1016, '204', 'BANCO S.R.L S.A', NULL),
(1017, '205', 'BANCO SUL AMERICA S.A', NULL),
(1018, '206', 'BANCO MARTINELLI S.A', NULL),
(1019, '208', 'BANCO PACTUAL S.A', NULL),
(1020, '210', 'DEUTSCH SUDAMERIKANICHE BANK AG', NULL),
(1021, '211', 'BANCO SISTEMA S.A', NULL),
(1022, '212', 'BANCO MATONE S.A', NULL),
(1023, '213', 'BANCO ARBI S.A', NULL),
(1024, '214', 'BANCO DIBENS S.A', NULL),
(1025, '215', 'BANCO AMERICA DO SUL S.A', NULL),
(1026, '216', 'BANCO REGIONAL MALCON S.A', NULL),
(1027, '217', 'BANCO AGROINVEST S.A', NULL),
(1028, '218', 'BBS - BANCO BONSUCESSO S.A.', NULL),
(1029, '219', 'BANCO DE CREDITO DE SAO PAULO S.A', NULL),
(1030, '220', 'BANCO CREFISUL', NULL),
(1031, '221', 'BANCO GRAPHUS S.A', NULL),
(1032, '222', 'BANCO AGF BRASIL S. A.', NULL),
(1033, '223', 'BANCO INTERUNION S.A', NULL),
(1034, '224', 'BANCO FIBRA S.A', NULL),
(1035, '225', 'BANCO BRASCAN S.A', NULL),
(1036, '228', 'BANCO ICATU S.A', NULL),
(1037, '229', 'BANCO CRUZEIRO S.A', NULL),
(1038, '230', 'BANCO BANDEIRANTES S.A', NULL),
(1039, '231', 'BANCO BOAVISTA S.A', NULL),
(1040, '232', 'BANCO INTERPART S.A', NULL),
(1041, '233', 'BANCO MAPPIN S.A', NULL),
(1042, '234', 'BANCO LAVRA S.A.', NULL),
(1043, '235', 'BANCO LIBERAL S.A', NULL),
(1044, '236', 'BANCO CAMBIAL S.A', NULL),
(1045, '237', 'BANCO BRADESCO S.A', NULL),
(1046, '239', 'BANCO BANCRED S.A', NULL),
(1047, '240', 'BANCO DE CREDITO REAL DE MINAS GERAIS S.', NULL),
(1048, '241', 'BANCO CLASSICO S.A', NULL),
(1049, '242', 'BANCO EUROINVEST S.A', NULL),
(1050, '243', 'BANCO STOCK S.A', NULL),
(1051, '244', 'BANCO CIDADE S.A', NULL),
(1052, '245', 'BANCO EMPRESARIAL S.A', NULL),
(1053, '246', 'BANCO ABC ROMA S.A', NULL),
(1054, '247', 'BANCO OMEGA S.A', NULL),
(1055, '249', 'BANCO INVESTCRED S.A', NULL),
(1056, '250', 'BANCO SCHAHIN CURY S.A', NULL),
(1057, '251', 'BANCO SAO JORGE S.A.', NULL),
(1058, '252', 'BANCO FININVEST S.A', NULL),
(1059, '254', 'BANCO PARANA BANCO S.A', NULL),
(1060, '255', 'MILBANCO S.A.', NULL),
(1061, '256', 'BANCO GULVINVEST S.A', NULL),
(1062, '258', 'BANCO INDUSCRED S.A', NULL),
(1063, '261', 'BANCO VARIG S.A', NULL),
(1064, '262', 'BANCO BOREAL S.A', NULL),
(1065, '263', 'BANCO CACIQUE', NULL),
(1066, '264', 'BANCO PERFORMANCE S.A', NULL),
(1067, '265', 'BANCO FATOR S.A', NULL),
(1068, '266', 'BANCO CEDULA S.A', NULL),
(1069, '267', 'BANCO BBM-COM.C.IMOB.CFI S.A.', NULL),
(1070, '275', 'BANCO REAL S.A', NULL),
(1071, '277', 'BANCO PLANIBANC S.A', NULL),
(1072, '282', 'BANCO BRASILEIRO COMERCIAL', NULL),
(1073, '291', 'BANCO DE CREDITO NACIONAL S.A', NULL),
(1074, '294', 'BCR - BANCO DE CREDITO REAL S.A', NULL),
(1075, '295', 'BANCO CREDIPLAN S.A', NULL),
(1076, '300', 'BANCO DE LA NACION ARGENTINA S.A', NULL),
(1077, '302', 'BANCO DO PROGRESSO S.A', NULL),
(1078, '303', 'BANCO HNF S.A.', NULL),
(1079, '304', 'BANCO PONTUAL S.A', NULL),
(1080, '308', 'BANCO COMERCIAL BANCESA S.A.', NULL),
(1081, '318', 'BANCO B.M.G. S.A', NULL),
(1082, '320', 'BANCO INDUSTRIAL E COMERCIAL', NULL),
(1083, '341', 'BANCO ITAU S.A', NULL),
(1084, '346', 'BANCO FRANCES E BRASILEIRO S.A', NULL),
(1085, '347', 'BANCO SUDAMERIS BRASIL S.A', NULL),
(1086, '351', 'BANCO BOZANO SIMONSEN S.A', NULL),
(1087, '353', 'BANCO GERAL DO COMERCIO S.A', NULL),
(1088, '356', 'ABN AMRO S.A', NULL),
(1089, '366', 'BANCO SOGERAL S.A', NULL),
(1090, '369', 'PONTUAL', NULL),
(1091, '370', 'BEAL - BANCO EUROPEU PARA AMERICA LATINA', NULL),
(1092, '372', 'BANCO ITAMARATI S.A', NULL),
(1093, '375', 'BANCO FENICIA S.A', NULL),
(1094, '376', 'CHASE MANHATTAN BANK S.A', NULL),
(1095, '388', 'BANCO MERCANTIL DE DESCONTOS S/A', NULL),
(1096, '389', 'BANCO MERCANTIL DO BRASIL S.A', NULL),
(1097, '392', 'BANCO MERCANTIL DE SAO PAULO S.A', NULL),
(1098, '394', 'BANCO B.M.C. S.A', NULL),
(1099, '399', 'BANCO BAMERINDUS DO BRASIL S.A', NULL),
(1100, '409', 'UNIBANCO - UNIAO DOS bank BRASILEIROS', NULL),
(1101, '412', 'BANCO NACIONAL DA BAHIA S.A', NULL),
(1102, '415', 'BANCO NACIONAL S.A', NULL),
(1103, '420', 'BANCO NACIONAL DO NORTE S.A', NULL),
(1104, '422', 'BANCO SAFRA S.A', NULL),
(1105, '424', 'BANCO NOROESTE S.A', NULL),
(1106, '434', 'BANCO FORTALEZA S.A', NULL),
(1107, '453', 'BANCO RURAL S.A', NULL),
(1108, '456', 'BANCO TOKIO S.A', NULL),
(1109, '464', 'BANCO SUMITOMO BRASILEIRO S.A', NULL),
(1110, '466', 'BANCO MITSUBISHI BRASILEIRO S.A', NULL),
(1111, '472', 'LLOYDS BANK PLC', NULL),
(1112, '473', 'BANCO FINANCIAL PORTUGUES S.A', NULL),
(1113, '477', 'CITIBANK N.A', NULL),
(1114, '479', 'BANCO DE BOSTON S.A', NULL),
(1115, '480', 'BANCO PORTUGUES DO ATLANTICO-BRASIL S.A', NULL),
(1116, '483', 'BANCO AGRIMISA S.A.', NULL),
(1117, '487', 'DEUTSCHE BANK S.A - BANCO ALEMAO', NULL),
(1118, '488', 'BANCO J. P. MORGAN S.A', NULL),
(1119, '489', 'BANESTO BANCO URUGAUAY S.A', NULL),
(1120, '492', 'INTERNATIONALE NEDERLANDEN BANK N.V.', NULL),
(1121, '493', 'BANCO UNION S.A.C.A', NULL),
(1122, '494', 'BANCO LA REP. ORIENTAL DEL URUGUAY', NULL),
(1123, '495', 'BANCO LA PROVINCIA DE BUENOS AIRES', NULL),
(1124, '496', 'BANCO EXTERIOR DE ESPANA S.A', NULL),
(1125, '498', 'CENTRO HISPANO BANCO', NULL),
(1126, '499', 'BANCO IOCHPE S.A', NULL),
(1127, '501', 'BANCO BRASILEIRO IRAQUIANO S.A.', NULL),
(1128, '502', 'BANCO SANTANDER S.A', NULL),
(1129, '504', 'BANCO MULTIPLIC S.A', NULL),
(1130, '505', 'BANCO GARANTIA S.A', NULL),
(1131, '600', 'BANCO LUSO BRASILEIRO S.A', NULL),
(1132, '601', 'BFC BANCO S.A.', NULL),
(1133, '602', 'BANCO PATENTE S.A', NULL),
(1134, '604', 'BANCO INDUSTRIAL DO BRASIL S.A', NULL),
(1135, '607', 'BANCO SANTOS NEVES S.A', NULL),
(1136, '608', 'BANCO OPEN S.A', NULL),
(1137, '610', 'BANCO V.R. S.A', NULL),
(1138, '611', 'BANCO PAULISTA S.A', NULL),
(1139, '612', 'BANCO GUANABARA S.A', NULL),
(1140, '613', 'BANCO PECUNIA S.A', NULL),
(1141, '616', 'BANCO INTERPACIFICO S.A', NULL),
(1142, '617', 'BANCO INVESTOR S.A.', NULL),
(1143, '618', 'BANCO TENDENCIA S.A', NULL),
(1144, '621', 'BANCO APLICAP S.A.', NULL),
(1145, '622', 'BANCO DRACMA S.A', NULL),
(1146, '623', 'BANCO PANAMERICANO S.A', NULL),
(1147, '624', 'BANCO GENERAL MOTORS S.A', NULL),
(1148, '625', 'BANCO ARAUCARIA S.A', NULL),
(1149, '626', 'BANCO FICSA S.A', NULL),
(1150, '627', 'BANCO DESTAK S.A', NULL),
(1151, '628', 'BANCO CRITERIUM S.A', NULL),
(1152, '629', 'BANCORP BANCO COML. E. DE INVESTMENTO', NULL),
(1153, '630', 'BANCO INTERCAP S.A', NULL),
(1154, '633', 'BANCO REDIMENTO S.A', NULL),
(1155, '634', 'BANCO TRIANGULO S.A', NULL),
(1156, '635', 'BANCO DO ESTADO DO AMAPA S.A', NULL),
(1157, '637', 'BANCO SOFISA S.A', NULL),
(1158, '638', 'BANCO PROSPER S.A', NULL),
(1159, '639', 'BIG S.A. - BANCO IRMAOS GUIMARAES', NULL),
(1160, '640', 'BANCO DE CREDITO METROPOLITANO S.A', NULL),
(1161, '641', 'BANCO EXCEL ECONOMICO S/A', NULL),
(1162, '643', 'BANCO SEGMENTO S.A', NULL),
(1163, '645', 'BANCO DO ESTADO DE RORAIMA S.A', NULL),
(1164, '647', 'BANCO MARKA S.A', NULL),
(1165, '648', 'BANCO ATLANTIS S.A', NULL),
(1166, '649', 'BANCO DIMENSAO S.A', NULL),
(1167, '650', 'BANCO PEBB S.A', NULL),
(1168, '652', 'BANCO FRANCES E BRASILEIRO SA', NULL),
(1169, '653', 'BANCO INDUSVAL S.A', NULL),
(1170, '654', 'BANCO A. J. RENNER S.A', NULL),
(1171, '655', 'BANCO VOTORANTIM S.A.', NULL),
(1172, '656', 'BANCO MATRIX S.A', NULL),
(1173, '657', 'BANCO TECNICORP S.A', NULL),
(1174, '658', 'BANCO PORTO REAL S.A', NULL),
(1175, '702', 'BANCO SANTOS S.A', NULL),
(1176, '705', 'BANCO INVESTCORP S.A.', NULL),
(1177, '707', 'BANCO DAYCOVAL S.A', NULL),
(1178, '711', 'BANCO VETOR S.A.', NULL),
(1179, '713', 'BANCO CINDAM S.A', NULL),
(1180, '715', 'BANCO VEGA S.A', NULL),
(1181, '718', 'BANCO OPERADOR S.A', NULL),
(1182, '719', 'BANCO PRIMUS S.A', NULL),
(1183, '720', 'BANCO MAXINVEST S.A', NULL),
(1184, '721', 'BANCO CREDIBEL S.A', NULL),
(1185, '722', 'BANCO INTERIOR DE SAO PAULO S.A', NULL),
(1186, '724', 'BANCO PORTO SEGURO S.A', NULL),
(1187, '725', 'BANCO FINABANCO S.A', NULL),
(1188, '726', 'BANCO UNIVERSAL S.A', NULL),
(1189, '728', 'BANCO FITAL S.A', NULL),
(1190, '729', 'BANCO FONTE S.A', NULL),
(1191, '730', 'BANCO COMERCIAL PARAGUAYO S.A', NULL),
(1192, '731', 'BANCO GNPP S.A.', NULL),
(1193, '732', 'BANCO PREMIER S.A.', NULL),
(1194, '733', 'BANCO NACOES S.A.', NULL),
(1195, '734', 'BANCO GERDAU S.A', NULL),
(1196, '735', 'BACO POTENCIAL', NULL),
(1197, '736', 'BANCO UNITED S.A', NULL),
(1198, '737', 'THECA', NULL),
(1199, '738', 'MARADA', NULL),
(1200, '739', 'BGN', NULL),
(1201, '740', 'BCN BARCLAYS', NULL),
(1202, '741', 'BRP', NULL),
(1203, '742', 'EQUATORIAL', NULL),
(1204, '743', 'BANCO EMBLEMA S.A', NULL),
(1205, '744', 'THE FIRST NATIONAL BANK OF BOSTON', NULL),
(1206, '745', 'CITIBAN N.A.', NULL),
(1207, '746', 'MODAL SA', NULL),
(1208, '747', 'RAIBOBANK DO BRASIL', NULL),
(1209, '748', 'SICREDI', NULL),
(1210, '749', 'BRMSANTIL SA', NULL),
(1211, '750', 'BANCO REPUBLIC NATIONAL OF NEW YORK (BRA', NULL),
(1212, '751', 'DRESDNER BANK LATEINAMERIKA-BRASIL S/A', NULL),
(1213, '752', 'BANCO BANQUE NATIONALE DE PARIS BRASIL S', NULL),
(1214, '753', 'BANCO COMERCIAL URUGUAI S.A.', NULL),
(1215, '755', 'BANCO MERRILL LYNCH S.A', NULL),
(1216, '756', 'BANCO COOPERATIVO DO BRASIL S.A.', NULL),
(1217, '757', 'BANCO KEB DO BRASIL S.A.', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `deletado` int(11) DEFAULT NULL,
  `thumb` varchar(300) DEFAULT NULL,
  `nomefoto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`id`, `nome`, `deletado`, `thumb`, `nomefoto`) VALUES
(1, 'Matemática', NULL, 'https://www.cetecc.org.br/wp-content/uploads/2018/04/58521.jpg', 'matematica'),
(2, 'Português', NULL, 'https://conteudo.imguol.com.br/blogs/150/files/2015/09/abc.jpg', 'portugues'),
(3, 'Física', NULL, 'https://abrilguiadoestudante.files.wordpress.com/2017/07/fisica-mecanica-1.jpg', 'fisica'),
(4, 'Química', NULL, 'http://casaamericana.com.br/wp-content/uploads/2018/11/a-quimica-do-nosso-dia-a-dia.jpg', 'quimica'),
(8, 'Biologia', NULL, 'https://s2.glbimg.com/6xL7QPe9YMSm8N-aonWJQFXifH0=/e.glbimg.com/og/ed/f/original/2018/10/29/dna-3539309_1920.jpg', 'biologia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lancamento`
--

CREATE TABLE `lancamento` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `data` datetime DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `qtdaula` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `id_tipolancamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lancamento`
--

INSERT INTO `lancamento` (`id`, `id_usuario`, `data`, `valor`, `qtdaula`, `tipo`, `id_tipolancamento`) VALUES
(1, 1, '2020-01-02 21:44:15', NULL, 1, NULL, 4),
(2, 1, '2020-01-02 21:45:21', NULL, 1, NULL, 4),
(3, 1, '2020-01-02 21:52:00', NULL, 1, NULL, 4),
(4, 1, '2020-01-02 21:54:44', NULL, 1, NULL, 4),
(5, 1, '2020-01-02 21:58:47', NULL, 1, NULL, 4),
(6, 1, '2020-01-02 22:00:15', NULL, 1, NULL, 4),
(7, 1, '2020-01-02 22:01:38', NULL, 1, NULL, 4),
(8, 1, '2020-01-02 22:04:26', NULL, 1, NULL, 4),
(9, 1, '2020-01-02 22:07:38', NULL, 1, NULL, 4),
(10, 1, '2020-01-02 22:08:40', NULL, 1, NULL, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `deletado` int(11) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `subcategoria`
--

INSERT INTO `subcategoria` (`id`, `nome`, `deletado`, `id_categoria`) VALUES
(1, 'Algebra 1', NULL, 1),
(2, 'Algebra 2', NULL, 1),
(3, 'Geometria', NULL, 1),
(4, 'Morfologia', NULL, 2),
(5, 'Sintaxe', NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipolancamento`
--

CREATE TABLE `tipolancamento` (
  `id` int(11) NOT NULL,
  `descricao` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipolancamento`
--

INSERT INTO `tipolancamento` (`id`, `descricao`) VALUES
(1, 'Crédito Aluno'),
(2, 'Crédito Professor'),
(3, 'Crédito Studylive'),
(4, 'Pagamento Aula'),
(5, 'Pagamento Professor'),
(6, 'Estorno Aula Aluno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipousuario`
--

CREATE TABLE `tipousuario` (
  `id` int(11) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipousuario`
--

INSERT INTO `tipousuario` (`id`, `descricao`) VALUES
(1, 'Aluno'),
(2, 'Professor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) DEFAULT NULL,
  `apelido` varchar(200) DEFAULT NULL,
  `login` varchar(100) DEFAULT NULL,
  `senha` varchar(200) DEFAULT NULL,
  `id_tipousuario` int(11) NOT NULL,
  `saldo` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT NULL,
  `confirmouemail` int(1) DEFAULT NULL,
  `email` varchar(180) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `rg` varchar(45) DEFAULT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `avatar` text DEFAULT NULL,
  `bgimage` text DEFAULT NULL,
  `id_banco` int(11) DEFAULT NULL,
  `agencia` varchar(20) DEFAULT NULL,
  `tipoconta` int(1) DEFAULT NULL COMMENT '1- CC, 2-POUPANCA',
  `contacorrente` varchar(25) DEFAULT NULL,
  `poupanca` varchar(25) DEFAULT NULL,
  `variacaopoupanca` varchar(15) DEFAULT NULL,
  `logradouro` varchar(250) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro` varchar(150) DEFAULT NULL,
  `cidade` varchar(150) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  `cep` varchar(12) DEFAULT NULL,
  `configurouconta` int(1) DEFAULT NULL,
  `avaliacaoMedia` float DEFAULT NULL,
  `token_fcm` varchar(500) DEFAULT NULL,
  `codativacao` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `apelido`, `login`, `senha`, `id_tipousuario`, `saldo`, `ativo`, `confirmouemail`, `email`, `cpf`, `rg`, `dt_nascimento`, `celular`, `avatar`, `bgimage`, `id_banco`, `agencia`, `tipoconta`, `contacorrente`, `poupanca`, `variacaopoupanca`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `cep`, `configurouconta`, `avaliacaoMedia`, `token_fcm`, `codativacao`) VALUES
(1, 'Maurício Ferg', 'Ferg', 'ferg', 'c39e1a03859f9ee215bc49131d0caf33', 1, 82, NULL, NULL, 'mauriciogarcia81@hotmail.com', '', '', '1988-01-22', '(32) 32323-5656', '1571789106', '1571790678', 0, '', 0, '', '', '', 'Rua Francisco Chagas', '138', '', 'Glória', 'Vila Velha', 'ES', '29122-380', 1, NULL, 'cQPHE8IcKxw:APA91bHgl1KchiaKg8aZFC0jkZYgk-ApD0-DKDGgmN50rG5BhaZ90d1uN4Mal5AIMKWSCYKOe6SQFlYQL9jneF-kHl5XUHSN9eGE-PjKkYLp6nO9_1aK7XJbtN9Yz5MpfIoHaXLN-Q3w', NULL),
(2, 'Priscila Baiense', 'Pri2', 'pris', '123', 2, NULL, 1, 1, 'pris.baiense@hotmail.com', '545.454.545-45', '', '2019-10-08', '(32) 3232-32', '1572698755', NULL, 974, '8484', 1, '737374', '', '', 'Rua Francisco Chagas', '123', '646r', 'Glória', 'Vila Velha', 'ES', '29122-380', 1, 3.5, 'cQPHE8IcKxw:APA91bHgl1KchiaKg8aZFC0jkZYgk-ApD0-DKDGgmN50rG5BhaZ90d1uN4Mal5AIMKWSCYKOe6SQFlYQL9jneF-kHl5XUHSN9eGE-PjKkYLp6nO9_1aK7XJbtN9Yz5MpfIoHaXLN-Q3w', NULL),
(3, 'Tiago Gava', 'Gava', 'tiago', '123', 2, NULL, NULL, NULL, 'tiago@studylive.com', '646.464.632-32', '', '2018-12-12', '(27) 94875-4875', '1575840165', NULL, 970, '7374', 1, '7373', '', '', 'Rua Francisco Chagas', '63', '', 'Glória', 'Vila Velha', 'ES', '29122-380', 1, NULL, NULL, NULL),
(4, 'Maria Fernanda Garcia Correa', NULL, 'nandagfaria', 'cd755a6c6b699f3262bcc2aa46ab507e', 1, NULL, NULL, NULL, 'aaa@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'ahshshsid', NULL, 'jjs', '123', 1, NULL, NULL, NULL, 'hdhdhd@jshd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'hwhehe', NULL, 'aaaa', '123', 1, NULL, NULL, NULL, 'hshs@fff.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'hahaha', NULL, 'foi1', '202cb962ac59075b964b07152d234b70', 1, NULL, NULL, NULL, 'serdiferenciado81@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1578193059');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuariocategoria`
--

CREATE TABLE `usuariocategoria` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuariosubcategoria`
--

CREATE TABLE `usuariosubcategoria` (
  `id_usuario` int(11) NOT NULL,
  `id_subcategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuariosubcategoria`
--

INSERT INTO `usuariosubcategoria` (`id_usuario`, `id_subcategoria`) VALUES
(1, 1),
(1, 2),
(1, 4),
(2, 2),
(3, 1),
(3, 2),
(3, 3);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `assunto`
--
ALTER TABLE `assunto`
  ADD PRIMARY KEY (`id`,`id_subcategoria`),
  ADD KEY `fk_assunto_subcategoria1_idx` (`id_subcategoria`);

--
-- Índices para tabela `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id`,`id_usuario_aluno`,`id_usuario_professor`),
  ADD KEY `fk_mural_usuario1_idx` (`id_usuario_aluno`);

--
-- Índices para tabela `aulaassunto`
--
ALTER TABLE `aulaassunto`
  ADD PRIMARY KEY (`id`,`id_aula`,`id_assunto`),
  ADD KEY `fk_muralassunto_mural1_idx` (`id_aula`),
  ADD KEY `fk_muralassunto_assunto1_idx` (`id_assunto`);

--
-- Índices para tabela `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `lancamento`
--
ALTER TABLE `lancamento`
  ADD PRIMARY KEY (`id`,`id_usuario`,`id_tipolancamento`),
  ADD KEY `fk_lancamento_usuario1_idx` (`id_usuario`),
  ADD KEY `fk_lancamento_tipolancamento1_idx` (`id_tipolancamento`);

--
-- Índices para tabela `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id`,`id_categoria`),
  ADD KEY `fk_subcategoria_categoria1_idx` (`id_categoria`);

--
-- Índices para tabela `tipolancamento`
--
ALTER TABLE `tipolancamento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tipousuario`
--
ALTER TABLE `tipousuario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`,`id_tipousuario`),
  ADD KEY `fk_usuario_tipousuario_idx` (`id_tipousuario`);

--
-- Índices para tabela `usuariocategoria`
--
ALTER TABLE `usuariocategoria`
  ADD PRIMARY KEY (`id`,`id_usuario`,`id_categoria`),
  ADD KEY `fk_usuariocategoria_usuario1_idx` (`id_usuario`),
  ADD KEY `fk_usuariocategoria_categoria1_idx` (`id_categoria`);

--
-- Índices para tabela `usuariosubcategoria`
--
ALTER TABLE `usuariosubcategoria`
  ADD PRIMARY KEY (`id_usuario`,`id_subcategoria`),
  ADD KEY `fk_usuariosubcategoria_subcategoria2_idx` (`id_subcategoria`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `assunto`
--
ALTER TABLE `assunto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `aula`
--
ALTER TABLE `aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT de tabela `aulaassunto`
--
ALTER TABLE `aulaassunto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de tabela `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1218;

--
-- AUTO_INCREMENT de tabela `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `lancamento`
--
ALTER TABLE `lancamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `tipolancamento`
--
ALTER TABLE `tipolancamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `tipousuario`
--
ALTER TABLE `tipousuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `usuariocategoria`
--
ALTER TABLE `usuariocategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `assunto`
--
ALTER TABLE `assunto`
  ADD CONSTRAINT `fk_assunto_subcategoria1` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `aula`
--
ALTER TABLE `aula`
  ADD CONSTRAINT `fk_mural_usuario1` FOREIGN KEY (`id_usuario_aluno`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `aulaassunto`
--
ALTER TABLE `aulaassunto`
  ADD CONSTRAINT `fk_muralassunto_assunto1` FOREIGN KEY (`id_assunto`) REFERENCES `assunto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_muralassunto_mural1` FOREIGN KEY (`id_aula`) REFERENCES `aula` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `lancamento`
--
ALTER TABLE `lancamento`
  ADD CONSTRAINT `fk_lancamento_tipolancamento1` FOREIGN KEY (`id_tipolancamento`) REFERENCES `tipolancamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lancamento_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `fk_subcategoria_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_tipousuario` FOREIGN KEY (`id_tipousuario`) REFERENCES `tipousuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuariocategoria`
--
ALTER TABLE `usuariocategoria`
  ADD CONSTRAINT `fk_usuariocategoria_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuariocategoria_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuariosubcategoria`
--
ALTER TABLE `usuariosubcategoria`
  ADD CONSTRAINT `fk_usuariosubcategoria_subcategoria2` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuariosubcategoria_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
