const Database = require('../database');
const firebase = require('../firebase');
FindTeacher = async (aula, arrayProf) => {
  var db = new Database;
  const subcategorias = await db.query( 
    `SELECT subcategoria.id
    FROM   aulaassunto
    JOIN   assunto on (assunto.id = aulaassunto.id_assunto)
    JOIN   subcategoria on (subcategoria.id = assunto.id_subcategoria)
    WHERE  aulaassunto.id_aula = ${aula.id}
    GROUP BY subcategoria.id ASC` 
  );
  var id_professor = 0;
  var tokenFcm = '';
  return firebase.database().ref(subcategorias[0].id).once("value", async (snapshot) => {
    snapshot.forEach( (childSnapshot) => {
      if (!arrayProf.includes(childSnapshot.key)) {
      // if (childSnapshot.key == 4) {
        id_professor = childSnapshot.key;
        tokenFcm = childSnapshot.child('tokenFcm').val();
        return true;
      }
    });
    
  }).then(() => {
    return {tokenFcm, id_professor};
  });
  
  
}

