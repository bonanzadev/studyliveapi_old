const Database = require('../database');
require('../utils/push');

module.exports = {
    async estouEmAula(req,res){

        var db = new Database;

        const id = req.params.id || 0;

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;

        const aula = await db.query( `  SELECT *, DATE_FORMAT(aula.dt_fim, "%Y-%m-%d %H:%i") as dtfim
                                        FROM   aula
                                        WHERE  ((id_usuario_aluno = ${id}) OR (id_usuario_professor = ${id}))
                                        AND    now() between dt_ini and dt_fim`);

        if( aula[0] !== undefined ){

            var start_date = moment( new Date(),'YYYY-MM-DD HH:mm');
            var end_date = moment( aula[0].dtfim, 'YYYY-MM-DD HH:mm');
            var duration = moment.duration(end_date.diff(start_date));
            var resultado = duration.asSeconds();     

            Object.assign(aula[0], { contadorEmSegundos: Math.round(resultado) });

            return res.json({ data: aula[0], errors: ""});
        } else {
            return res.json({ data: "", errors: ""});
        }
    },

    async suaAulaEstaProxima(req,res){

        var db = new Database;

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;

        const data = moment( new Date() ).add(60,'m').format('YYYY-MM-DD HH:mm:00');

        const aula = await db.query( `  SELECT *
                                        FROM   aula
                                        WHERE  '${data}' = dt_ini
                                        AND    aconteceu is null`);

        if( aula[0] !== undefined ){
            if( aula[0].id !== "" ){
                // console.log('Push enviado!')
                /* Enviar push aqui */
                /* Olá {{ FULANO }}, Faltam apenas 1 hora para sua aula, não perca o horário ;) */
            }
        }

        return res.json({ data: "", errors: ""});
    },
    async entrarSalaEspera(req,res){

        var db = new Database;

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;

        const data = moment( new Date() ).add(10,'m').format('YYYY-MM-DD HH:mm:00');
        const data2 = moment( new Date() ).add(5,'m').format('YYYY-MM-DD HH:mm:00');

        const aula = await db.query( `  SELECT aula.id ,aula.mensagem,cat.nome as displayCategoria, 
                                                us2.avatar, us2.id as id_usuario_aluno, us2.nome as nomeAluno,
                                                aula.id_usuario_professor, aula.channel, us.avatar as avatarProfessor,
                                                DATE_FORMAT(aula.dt_ini, "%H:%i") as dtinicio,
                                                us.nome as nomeProfessor,
                                                us.token_fcm as tokenFcm_professor, 
                                                us2.token_fcm as tokenFcm_aluno, 
                                                aula.qtdaula
                                        FROM aula
                                        left join usuario us on (us.id = aula.id_usuario_professor)
                                        join usuario us2 on (us2.id = aula.id_usuario_aluno)
                                        join aulaassunto aa on (aula.id = aa.id_aula)
                                        join assunto ass on (ass.id = aa.id_assunto)
                                        join subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join categoria cat on (sub.id_categoria = cat.id)
                                        WHERE  ( '${data}' = aula.dt_ini OR '${data2}' = aula.dt_ini )
                                        AND    aula.aconteceu is null
                                        GROUP BY aula.id
                                        `);
        for (let i = 0; i < aula.length; i++) {
            if( aula[i] !== undefined ){
                if( aula[i].id !== "" ){    
                    var start_date = moment( new Date(),'HH:mm');
                    var end_date = moment( aula[i].dtinicio, 'HH:mm');
                    var duration = moment.duration(end_date.diff(start_date));
                    var resultado = duration.asSeconds();     
                
                    const ass = await db.query( `  SELECT assunto.id, assunto.nome,aa.id_aula
                                                    FROM   aulaassunto aa
                                                    join   assunto on (assunto.id = aa.id_assunto)
                                                    where  aa.id_aula = ${aula[i].id}
                                                    order by aa.id asc` );
                    //console.log(ass);
                    Object.assign(aula[i], {assuntos: ass, origem: 'agendamento', contadorEmSegundos: Math.round(resultado)} );
    
                    // console.log( 'obj: ',aula[i] )
                    
                
                    console.log('Aula: ', aula[i]);
                    /* {ENVIAR PUSH FIREBASE AQUI} */
                    await sendPush(aula[i], [aula[i].tokenFcm_professor,aula[i].tokenFcm_aluno]);
                    
                    /* Enviar push aqui */
                    /* Chamar função que envia para sala de espera */
                }
            }
        }
        return res.json({ data: "", errors: ""});
        
    },

    async estamosSentidoSuaFalta(req,res){

        var db = new Database;

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;
        

        const data = moment( new Date() ).add(10,'m').format('YYYY-MM-DD HH:mm:00');

        const aula = await db.query( `  SELECT *
                                        FROM   aula
                                        WHERE  '${data}' = dt_ini
                                        AND    aconteceu is null`);

        if( aula[0] !== undefined ){
            if( aula[0].id !== "" ){
                console.log('Push enviado!')
                /* Enviar push aqui */
                /* Olá {{ FULANO }}, Faltam apenas 10 minutos para sua aula, não perca o horário ;) */
            }
        }

        return res.json({ data: "", errors: ""});
    },

   
}