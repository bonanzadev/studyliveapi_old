const Database = require('../database');
var table = "notificacao";

module.exports = {
    async index(req,res){

        const id = parseInt(req.params.id) || 0;

        var db = new Database;
        const resp = await db.query( `SELECT   ${table}.*, 
                                                DATE_FORMAT(dt_cadastro,'%d/%m/%Y') as dt_cadastro,
                                                tn.titulo, tn.icon
                                        FROM   ${table} 
                                        join   tiponotificacao tn on (tn.id = ${table}.id_tiponotificacao)
                                        where  ${table}.id_usuario = ${id} 
                                        order by id desc
                                        limit  8`).then( rows => rows );

        return res.json({ data: resp, errors: ""});
    },
    async qtd(req,res){

        const id = parseInt(req.params.id) || 0;

        var db = new Database;
        const resp = await db.query( `SELECT   count(1) as qtd
                                        FROM   ${table} 
                                        where  ${table}.id_usuario = ${id} 
                                        AND     lido is null
                                        order by id desc
                                        limit  8`).then( rows => rows );

        return res.json({ data: resp[0], errors: ""});
    },
    async lido(req,res){

        const id = parseInt(req.params.id) || 0;

        var db = new Database;
        const resp = await db.query( `UPDATE ${table} SET
                                            lido = 1
                                        where  ${table}.id_usuario = ${id}`).then( rows => rows );

        return res.json({ data: resp, errors: ""});
    },
}