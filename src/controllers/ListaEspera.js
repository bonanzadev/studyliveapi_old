const firebase = require('../firebase');

module.exports = {
    async index(req,res){
      var array = [];
      if(req.params.id){
        firebase.database().ref(req.params.id).on("value", (snapshot) => {
          snapshot.forEach( (childSnapshot) => {
            array.push(childSnapshot.key);
          });
        
        }, (errorObject) => {
          res.status(400).send('ops... algo deu errado.');
        });
        return res.json(array);
      }else{
        firebase.database().ref().on("value", (snapshot) => {
          snapshot.forEach( (childSnapshot) => {
            let arrayAux = []
            childSnapshot.forEach( (childChildSnapshot) => {
              // console.log(childSnapshot.key);
              arrayAux.push(childChildSnapshot.key);
            });
            array.splice(parseInt(childSnapshot.key), 0, arrayAux);

            // array[parseInt(childSnapshot.key)] = arrayAux;
            
            // var childData = childSnapshot.val()
          });
        
        }, (errorObject) => {
          res.status(400).send('ops... algo deu errado.');
        });
        return res.json(array);
      }
    },
    async delete(req,res){
      const id = req.params.id;
      if(req.params.id){
        firebase.database().ref().once("value", (snapshot) => {
          // console.log(snapshot.val());
          snapshot.forEach( (childSnapshot) => {
            // console.log(childSnapshot.ref.child(id).remove());
            // console.log("Pula");
            childSnapshot.ref.child(id).remove();
          });
          
        }, (errorObject) => {
          res.status(400).send('ops... algo deu errado.');
        });
        res.status(200).send('Usuário Removido com sucesso!');
      }else{
        res.status(400).send('Usuário não informado');
      }
    },
    async add(req,res){
      if(req.params.id && req.body.categorias){
        const id = req.params.id;
        const categorias = req.body.categorias;
        const nome = req.body.nome;
        const celular = req.body.celular;
        const tokenFcm = req.body.tokenFcm;
        categorias.forEach( (categoria) => {
          firebase.database().ref(categoria+"/"+id).once("value", (snapshot)=>{
            if(!snapshot.exists()){
              firebase.database().ref().child(categoria+"/"+id).set({
                'Celular': celular,
                'Nome': nome,
                'tokenFcm': tokenFcm,
                'qtd_cancela': 0
              });
              Date.now();
            }
          });
        });
        res.status(200).send('Usuário inserido com sucesso!');
      }else{
        res.status(400).send('Usuário ou categorias não informados');
      }
    },
    async edit(req,res){
      if(req.params.id){
        const id = req.params.id;
        firebase.database().ref().once("value", (snapshot) => {
          snapshot.forEach( (childSnapshot) => {
            let qtdCancela = parseInt( childSnapshot.child(id).child('qtd_cancela').val() );
            // console.log(childSnapshot);
            if(childSnapshot.child(id).exists()){
              // console.log(childSnapshot.key);
              firebase.database().ref(childSnapshot.key+"/"+id).update({
                'qtd_cancela': qtdCancela+1
              });
            }
          });
        
        }, (errorObject) => {
          res.status(400).send('ops... algo deu errado.');
        });
        res.status(200).send('Registro atualizado com sucesso!');
      }else{
        res.status(400).send('Usuário ou categorias não informados');
      }
    },
}