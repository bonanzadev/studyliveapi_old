const express = require('express');
const Usuario = require('./controllers/Usuario');
const Categoria = require('./controllers/Categoria');
const Subcategoria = require('./controllers/Subcategoria');
const Banco = require('./controllers/Banco');
const Assunto = require('./controllers/Assunto');
const Aula = require('./controllers/Aula');
const ListaEspera = require('./controllers/ListaEspera');
const Mail = require('./controllers/Mail');
const Notificacao = require('./controllers/Notificacao');
const Presenca = require('./controllers/Presenca');
const Central = require('./controllers/Central');
const router = express.Router();

router.get('/', (req, res) => res.json({ message: 'Funcionando!' }));

router.get('/suaAulaEstaProxima',Central.suaAulaEstaProxima);
router.get('/entrarSalaEspera',Central.entrarSalaEspera);
router.get('/estouEmAula/:id',Central.estouEmAula);

router.post('/presenca/:id',Presenca.add);

router.get('/notificacao/:id',Notificacao.index);
router.get('/qtdNotificacao/:id',Notificacao.qtd);
router.post('/notificacao/:id',Notificacao.lido);

router.get('/categoria/:id?',Categoria.index);
router.post('/categoria', Categoria.add);
router.delete('/categoria/:id',Categoria.delete);
router.put('/categoria/:id',Categoria.edit);

router.get('/Subcategoria/:id?',Subcategoria.index);
router.get('/Banco',Banco.index);
router.get('/SubcategoriaUsuario/:id_usuario?',Subcategoria.SubcategoriaUsuario);


router.get('/assunto/:categoria/:id?',Assunto.index);
router.delete('/assunto/:id',Assunto.delete);
router.post('/assunto',Assunto.add);
router.put('/assunto/:id',Assunto.edit);

router.get('/ativacao/:cod',Usuario.ativacao);
router.get('/esquecisenha/:email',Usuario.esquecisenha);
router.post('/cadastro',Usuario.cadastro);
router.post('/login',Usuario.login);
router.post('/avatar/:id',Usuario.setAvatar);
router.get('/avatar/:id',Usuario.getAvatar);
router.post('/bgimage/:id',Usuario.setBgimage);
router.get('/bgimage/:id',Usuario.getBgimage);
router.get('/usuario/:id',Usuario.index);
router.put('/usuario/:id',Usuario.update);
router.put('/updateToken/:id',Usuario.updateToken);

router.get('/muralAluno/:id?',Aula.muralAluno);
router.get('/muralProfessor/:id?',Aula.muralProfessor);
router.get('/historicoAulasAluno/:id?',Aula.historicoAulasAluno);
router.get('/agendamentosProfessor/:id?',Aula.agendamentosProfessor);
router.get('/startAula/:id?',Aula.startAula);
router.post('/setAulaProfessor/:id?',Aula.setAulaProfessor);
router.post('/cancelarAulaProfessor/:id?',Aula.cancelarAulaProfessor);
router.get('/loadaula/:id?', Aula.load);
router.post('/aula', Aula.add);
router.delete('/aula/:id',Aula.delete);
router.put('/aula/:id',Aula.edit);
router.post('/findTeacher/:id',Aula.findTeacher);
router.post('/descontaCreditoAluno/:id',Aula.descontaCreditoAluno);
router.post('/avaliacaoProfessor/:id',Aula.avaliacaoProfessor);

router.get('/listaEspera',ListaEspera.index);
router.get('/listaEspera/:id?',ListaEspera.index);
router.delete('/listaEspera/:id',ListaEspera.delete);
router.post('/listaEspera/:id', ListaEspera.add);
router.put('/listaEspera/:id',ListaEspera.edit);

router.post('/sendMail',Mail.index);
module.exports = router;
