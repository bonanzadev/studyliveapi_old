const Database = require('../database');

module.exports = {
    async add(req,res){

        var db = new Database;

        const id_aula = req.params.id || 0;
        const id_usuario = req.body.id_usuario || 0;
        var id_tipopresenca = req.body.id_tipopresenca || 0;

        if( id_tipopresenca == 1 ){
            /* Verifica se a aula já foi iniciada pelo usuário */
            const aula = await db.query( `SELECT count(1) as qtd FROM presenca 
                                            WHERE  id_aula = ${id_aula}
                                            AND    id_usuario = ${id_usuario}
                                            AND    id_tipopresenca = 1`);

            if( aula[0] !== undefined ){
                if( aula[0].qtd > 0 ){
                    /* Muda o tipo de presença para RECONECTOU */
                    id_tipopresenca = 4;
                }
            }
        }

        const resp = await db.query( `INSERT INTO presenca 
                                    (id_aula, id_usuario, id_tipopresenca, dt_presenca)
                                    VALUES(
                                     '${id_aula}',
                                     '${id_usuario}',
                                     '${id_tipopresenca}',
                                     now() )` );

        /* Verifica se a aula já foi iniciada pelo usuário */
        const aconteceu = await db.query( `SELECT  count(1) as qtd 
                                            FROM   aula 
                                            WHERE  id = ${id_aula}
                                            AND    aconteceu is null`);

        if( aconteceu[0] !== undefined ){
            if( aconteceu[0].qtd > 0 ){

                /* Verifica se já houve o presença do professor */
                const professor = await db.query( `  select count(1) as qtd
                                                    from presenca 
                                                    join aula on (aula.id = presenca.id_aula)
                                                    where presenca.id_aula = aula.id 
                                                    AND aula.id_usuario_professor = presenca.id_usuario 
                                                    AND presenca.id_tipopresenca = 2
                                                    AND presenca.id_aula = ${id_aula}`);

                /* Verifica se já houve o presença do aluno */
                const aluno = await db.query( `  select count(1) as qtd
                                                    from presenca 
                                                    join aula on (aula.id = presenca.id_aula)
                                                    where presenca.id_aula = aula.id 
                                                    AND aula.id_usuario_aluno = presenca.id_usuario 
                                                    AND presenca.id_tipopresenca = 2
                                                    AND presenca.id_aula = ${id_aula}`);

                if( professor[0] !== undefined && aluno[0] !== undefined ){
                    if( professor[0].qtd > 0 && aluno[0].qtd > 0 ){
                        /* Aula oficialmente aconteceu */
                        module.exports.aulaAconteceu(id_aula);
                    }
                }
            }
        }

        return res.json(resp);
    },

    /* Rotina Aula Aconteceu */
    async aulaAconteceu(id_aula){

        var db = new Database;

        await db.query( `UPDATE aula set aconteceu = 1 where id = ${id_aula}`);
        
        /* Pegando dados atuais da aula e do professor */
        const dadosAula = await db.query( ` SELECT aula.*, usuario.saldo
                                            FROM   usuario
                                            join   aula on (aula.id_usuario_professor = usuario.id)
                                            where  aula.id = ${id_aula}` );

        if( dadosAula[0] !== undefined ){

            /* Crédito já foi dado ao professor? */
            const lanc = await db.query( ` SELECT  count(1) as qtd
                                            FROM   lancamento
                                            where  id_aula = ${id_aula}
                                            AND    id_tipolancamento = 2 ` );

            if( lanc[0].qtd == 0 ){
                
                var newsaldo = dadosAula[0].saldo + dadosAula[0].qtdaula;
                var id_usuario_professor = dadosAula[0].id_usuario_professor;
                
                /* Inserindo Crédito Professor */
                await db.query( `  UPDATE usuario SET 
                                    saldo ='${newsaldo}'
                                    WHERE id=${id_usuario_professor}` );
                
                /* Inserindo lançamento Tipo 2 - Crédito Professor */
                const resp2 = await db.query( `  INSERT into lancamento 
                                                (id_usuario, data, qtdaula, id_tipolancamento, id_aula) 
                                                VALUES ('${id_usuario_professor}',now(),${dadosAula[0].qtdaula},2,${id_aula})` );

                if(resp2.affectedRows > 0){

                    return true;
                } else {

                    /* ENVIAR EMAIL SUPORTE */
                    return false;
                }   
            }                        
            
        } else {

            /* ENVIAR EMAIL SUPORTE */
            return false;
        }
    },
}