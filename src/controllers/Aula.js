const Database = require('../database');
require('../utils/push');
require('../utils/findTeacherFireBase');
require('../utils/email');

module.exports = {
    async muralAluno(req,res){

        const id = req.params.id || 0;

        var db = new Database;

        const aulas = await db.query( `  SELECT aula.*, us.nome, us.avatar, cat.nome as categoria,
                                        aula.dt_ini as dt_ini_data, 
                                        DATE_FORMAT(aula.dt_fim,'%H:%i') as dt_fim_hora 
                                        FROM   aula
                                        join   aulaassunto aa on (aula.id = aa.id_aula)
                                        join   assunto ass on (ass.id = aa.id_assunto)
                                        join   subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join   categoria cat on (sub.id_categoria = cat.id)
                                        left join usuario us on (us.id = aula.id_usuario_professor)
                                        where  aula.deletado is null
                                        and    aula.aconteceu is null
                                        and    now() < aula.dt_ini
                                        and    aula.id_usuario_aluno = ${id}
                                        group by aula.id
                                        order by aula.dt_ini asc` );

        for( var x=0;x<aulas.length;x++ ){
            const ass = await db.query( `  SELECT assunto.*,aa.id_aula
                                        FROM   aulaassunto aa
                                        join   assunto on (assunto.id = aa.id_assunto)
                                        where  aa.id_aula = ${aulas[x].id}
                                        order by aa.id asc` );
            
            
            Object.assign(aulas[x], {assuntos: ass});
        }
        
        const r = { aulas, errors: "" }
        return res.json(r);
    },
    async muralProfessor(req,res){
        
        const id = req.params.id || 0;

        var db = new Database;


        const aulas = await db.query( `  SELECT aula.*, us.nome, us.avatar, cat.nome as categoria,
                                        aula.dt_ini as dt_ini_data, 
                                        DATE_FORMAT(aula.dt_fim,'%H:%i') as dt_fim_hora 
                                        FROM   aula
                                        join   aulaassunto aa on (aula.id = aa.id_aula)
                                        join   assunto ass on (ass.id = aa.id_assunto)
                                        join   subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join   usuariosubcategoria ua on (ua.id_subcategoria = sub.id)
                                        join   usuario us on (us.id = aula.id_usuario_aluno)
                                        join   categoria cat on (sub.id_categoria = cat.id)
                                        where  aula.deletado is null
                                        and    aula.aconteceu is null
                                        and    now() < aula.dt_ini
                                        and    aula.id_usuario_professor = 0
                                        group by aula.id
                                        order by aula.dt_ini asc` );

        for( var x=0;x<aulas.length;x++ ){
            const ass = await db.query( `  SELECT assunto.*,aa.id_aula
                                        FROM   aulaassunto aa
                                        join   assunto on (assunto.id = aa.id_assunto)
                                        where  aa.id_aula = ${aulas[x].id}
                                        order by aa.id asc` );
            
            
            Object.assign(aulas[x], {assuntos: ass});
        }
        
        const r = { aulas, errors: "" }
        return res.json(r);
    },
    async agendamentosProfessor(req,res){
        
        const id = req.params.id || 0;

        var db = new Database;

        const aulas = await db.query( `  SELECT aula.*, us.nome, us.avatar, cat.nome as categoria,
                                        aula.dt_ini as dt_ini_data, 
                                        DATE_FORMAT(aula.dt_fim,'%H:%i') as dt_fim_hora 
                                        FROM   aula
                                        join   aulaassunto aa on (aula.id = aa.id_aula)
                                        join   assunto ass on (ass.id = aa.id_assunto)
                                        join   subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join   usuariosubcategoria ua on (ua.id_subcategoria = sub.id)
                                        join   usuario us on (us.id = aula.id_usuario_aluno)
                                        join   categoria cat on (sub.id_categoria = cat.id)
                                        where  aula.deletado is null
                                        and    aula.aconteceu is null
                                        and    aula.id_usuario_professor = ${id}
                                        group by aula.id
                                        order by aula.dt_ini asc` );

        for( var x=0;x<aulas.length;x++ ){
            const ass = await db.query( `  SELECT assunto.*,aa.id_aula
                                        FROM   aulaassunto aa
                                        join   assunto on (assunto.id = aa.id_assunto)
                                        where  aa.id_aula = ${aulas[x].id}
                                        order by aa.id asc` );
            
            
            Object.assign(aulas[x], {assuntos: ass});
        }
        
        const r = { aulas, errors: "" }
        return res.json(r);
    },
    async historicoAulasAluno(req,res){
        
        const id = req.params.id || 0;

        var db = new Database;

        const aulas = await db.query( ` SELECT aula.*, us.nome, us.avatar, cat.nome as categoria,
                                        aula.dt_ini as dt_ini_data, 
                                        DATE_FORMAT(aula.dt_fim,'%H:%i') as dt_fim_hora 
                                        FROM   aula
                                        join   aulaassunto aa on (aula.id = aa.id_aula)
                                        join   assunto ass on (ass.id = aa.id_assunto)
                                        join   subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join   categoria cat on (sub.id_categoria = cat.id)
                                        join   usuario us on (us.id = aula.id_usuario_professor)
                                        where  aula.deletado is null
                                        and    aula.aconteceu is not null
                            
                                        and    aula.id_usuario_aluno = ${id}
                            
                                        group by aula.id
                                        order by aula.dt_ini asc` );

        for( var x=0;x<aulas.length;x++ ){
            const ass = await db.query( `  SELECT assunto.*,aa.id_aula
                                        FROM   aulaassunto aa
                                        join   assunto on (assunto.id = aa.id_assunto)
                                        where  aa.id_aula = ${aulas[x].id}
                                        order by aa.id asc` );
            
            
            Object.assign(aulas[x], {assuntos: ass});
        }
        
        const r = { aulas, errors: "" }
        return res.json(r);
    },
    async cancelarAulaProfessor(req,res){
        
        var db = new Database;
        const resp = await db.query( 'update aula set id_usuario_professor = 0 WHERE ID=' + parseInt(req.params.id) ).then( rows => rows );

        return res.json(resp);
    },
    async delete(req,res){
        
        var db = new Database;
        const resp = await db.query( 'update aula set deletado = 1 WHERE ID=' + parseInt(req.params.id) ).then( rows => rows );

        return res.json(resp);
    },
    async add(req,res){
        
        var db = new Database;

        const id_usuario_aluno = req.body.id_usuario_aluno;
        const id_usuario_professor = req.body.id_usuario_professor  || 0;
        const qtdaula = req.body.qtdaula;
        const mensagem = req.body.mensagem.substring(0,200) || '';
        const assuntos = req.body.assuntos;
        const channel = id_usuario_aluno + "_" + Math.floor(Math.random() * (1000 - 1) + 1);

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;

        if( req.body.formatedDate !== undefined ){
            var agendamento = true;

            var dt_ini_moment = moment(req.body.formatedDate+" "+req.body.timeBR+":00").format('YYYY-MM-DD HH:mm:ss') || null;
            var dt_ini_select = moment(req.body.formatedDate+" "+req.body.timeBR+":00").format('DD/MM/YYYY - HH:mm') || null;
            var dt_ini = '"'+moment(req.body.formatedDate+" "+req.body.timeBR+":00").format('YYYY-MM-DD HH:mm:ss')+'"' || null;

            if( dt_ini != 'NULL'){
                var dt_fim = '"'+moment(dt_ini_moment).add((req.body.qtdaula*30), 'm').format('YYYY-MM-DD HH:mm:ss')+'"';
            } else {
                var dt_fim = null;
            }

            /* Verificação se já existe aula no periodo solicitado */
            const verificacao = await db.query( `   SELECT count(1) as existe 
                                                    FROM   aula
                                                    WHERE  id_usuario_aluno = ${id_usuario_aluno}
                                                    AND    ${dt_ini} between dt_ini AND dt_fim ` );

            
            if( verificacao[0].existe > 0 ){
                const result = { data: '', errors: "Já existe aula marcada nesse periodo" }
                return res.json(result);
            }

        } else {
            var agendamento = false;
            var dt_ini = null;
            var dt_fim = null;
        }

        const resp = await db.query( `INSERT INTO aula 
                                    (dt_ini,dt_fim,dt_cadastro,id_usuario_aluno,id_usuario_professor,qtdaula,channel,mensagem) 
                                    VALUES
                                    (${dt_ini},
                                     ${dt_fim},
                                     now(),
                                     '${id_usuario_aluno}',
                                     '${id_usuario_professor}',
                                     '${qtdaula}',
                                     '${channel}',
                                     '${mensagem}')` )
                                     
                                     .then( rows => {

                                        var id_aula = rows.insertId;
                                        
                                        if(id_aula != ""){
                                            for( var x=0;x<assuntos.length;x++ ){
                                                db.query(`INSERT INTO aulaassunto (id_aula,id_assunto) VALUES ('${id_aula}','${assuntos[x].id}')`).then( rows => rows );
                                            }
                                        }

                                        const descontar = module.exports.descontaCredito(id_aula,id_usuario_aluno);
                                        const qtd = (qtdaula > 1) ? "aulas" : "aula";

                                        if( agendamento ){
                                                db.query(`INSERT INTO notificacao 
                                                            ( descricao, id_usuario, dt_cadastro, lido, id_tiponotificacao)
                                                            VALUES 
                                                            (
                                                            'Você solicitou um agendamento de ${qtdaula} ${qtd} para ${dt_ini_select}. Avisaremos quando um professor responder sua solicitação',
                                                            '${id_usuario_aluno}',
                                                            now(),
                                                            NULL,
                                                            4
                                                            )`).then( rows => rows );
                                        }

                                        const result = { data: id_aula, errors: "" }
                                        return res.json(result);
                                         
                                     } );
        
        
    },
    async edit(req,res){

        const id = parseInt(req.params.id);
        const id_categoria = req.body.id_categoria;
        const dt_ini = req.body.dt_ini || '';
        const dt_fim = req.body.dt_fim || '';;
        const id_usuario_aluno = req.body.id_usuario_aluno;
        const id_usuario_professor = req.body.id_usuario_professor  || null;
        const qtdaula = req.body.qtdaula;
        const mensagem = req.body.mensagem.substring(0,200) || '';
        const channel = req.body.channel || '';
        const aconteceu = req.body.aconteceu || '';

        var db = new Database;

        const resp = await db.query( `  UPDATE aula SET 
                                        dt_ini = '${dt_ini}',
                                        dt_fim = '${dt_fim}',
                                        id_usuario_aluno ='${id_usuario_aluno}',
                                        id_usuario_professor ='${id_usuario_professor}',
                                        qtdaula ='${qtdaula}',
                                        mensagem ='${mensagem}',
                                        channel ='${channel}',
                                        aconteceu ='${aconteceu}',
                                        WHERE id=${id}` ).then( rows => rows );

        return res.json(resp);
    },
    /* LOAD NA AULA ! */
    async load(req,res){

        const id = parseInt(req.params.id);

        var db = new Database;
        

        const aulas = await db.query( ` SELECT  aula.*, usuario.avatar as avatarAluno, 
                                                us2.avatar as avatarProfessor,us2.nome as nomeProfessor,
                                                cat.nome as displayCategoria
                                        FROM    aula
                                        join    aulaassunto aa on (aula.id = aa.id_aula)
                                        join    assunto ass on (ass.id = aa.id_assunto)
                                        join    subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join    categoria cat on (sub.id_categoria = cat.id)
                                        join    usuario on (usuario.id = aula.id_usuario_aluno)
                                        
                                        left join usuario us2  on (us2.id = aula.id_usuario_professor)
                                        where   aula.id = ${id}` );

        if( aulas[0] != undefined ){
            var result = { data: aulas[0], errors: "" }
        } else {
            var result = { data: "", errors: "Aula não encontrada" }
        }
        
        return res.json(result);
    },
    /* AVALIAÇÃO DO PROFESSOR ! */
    async avaliacaoProfessor(req,res){

        const id = parseInt(req.params.id);
        const avaliacao = parseInt(req.body.avaliacao);
        const avaliacaoMsg = req.body.avaliacaoMsg;
        const id_usuario_professor = req.body.id_usuario_professor;

        var db = new Database;

        if( id == "" || id == undefined ){
            var result = { data: "", errors: "Aula não encontrada" }
            return res.json(resp);
        }

        const resp = await db.query( `  UPDATE aula SET 
                                        avaliacao ='${avaliacao}', avaliacaoMsg ='${avaliacaoMsg}'
                                        WHERE id=${id}` ).then( rows => rows );

        const aulas = await db.query( ` SELECT  avg(avaliacao) as media
                                        FROM    aula
                                        where   id_usuario_professor = ${id_usuario_professor}
                                        AND     avaliacao is not null 
                                        AND     avaliacao <> 0
                                        AND     aconteceu is not null` );

        if( aulas[0] != undefined ){
            const resp2 = await db.query( `  UPDATE usuario SET 
                                            avaliacaoMedia ='${aulas[0].media}'
                                            WHERE id=${id_usuario_professor}` ).then( rows => rows );
        }

        var result = { data: resp, errors: "" }
        return res.json(resp);
    },
    /* START NA AULA ! */
    async startAula(req,res){

        const id = parseInt(req.params.id);

        var db = new Database;

        const aulas = await db.query( ` SELECT *
                                        FROM   aula
                                        where  aula.id = ${id}` );

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;

        var dt_ini = moment().add(1,'m').format('YYYY-MM-DD HH:mm:ss');
        var dt_fim = moment(dt_ini).add((aulas[0].qtdaula*30),'m').format('YYYY-MM-DD HH:mm:ss');
        

        const resp = await db.query( `  UPDATE aula SET 
                                        dt_ini ='${dt_ini}', dt_fim ='${dt_fim}'
                                        WHERE id=${id}` ).then( rows => rows );

        return res.json(resp);
    },
    /* Chamar essa função quando o professor responder ao chamado de aula - ID AULA e ID PROFESSOR */
    async setAulaProfessor(req,res){

        const id = parseInt(req.params.id);
        const id_usuario_professor = req.body.id_usuario_professor;

        if( !req.body.id_usuario_professor ){
            const result = { data: '', errors: "Escolha o professor" }
            return res.json(result);
        }

        var db = new Database;

        const aulas = await db.query( ` SELECT      aula.*, DATE_FORMAT(aula.dt_ini, "%d/%m/%Y às %H:%i") as dtaula, 
                                                    us2.nome as nomeAluno, us2.apelido as apelidoAluno
                                        FROM        aula
                                        join        usuario us2 on (us2.id = aula.id_usuario_aluno)
                                        where       aula.id = ${id} 
                                        AND         (aula.id_usuario_professor = 0 OR aula.id_usuario_professor IS NULL)` );

        if( aulas[0] != undefined ){
            const resp = await db.query( `  UPDATE aula SET 
                                            id_usuario_professor ='${id_usuario_professor}'
                                            WHERE id=${id}` );

            const prof = await db.query( `  select * from usuario where id ='${id_usuario_professor}'` );

            await db.query(`INSERT INTO notificacao 
                            ( descricao, id_usuario, dt_cadastro, lido, id_tiponotificacao)
                            VALUES 
                            (
                            '${prof[0].apelido || prof[0].nome} será o seu professor(a) dia ${aulas[0].dtaula}.',
                            '${aulas[0].id_usuario_aluno}',
                            now(),
                            NULL,
                            2
                            )`);

            await db.query(`INSERT INTO notificacao 
                            ( descricao, id_usuario, dt_cadastro, lido, id_tiponotificacao)
                            VALUES 
                            (
                            '${aulas[0].apelidoAluno || aulas[0].nomeAluno} estará te esperando para aula no dia ${aulas[0].dtaula}.',
                            '${id_usuario_professor}',
                            now(),
                            NULL,
                            3
                            )`);

            return res.json(resp);
        } else {
            const result = { data: '', errors: "Aula já possui professor" }
            return res.json(result);
        }

        
    },

    /* DESCONTA CREDITO DO ALUNO */
    async descontaCreditoAluno(req,res){

        const id = parseInt(req.params.id);
        const id_usuario_aluno = req.body.id_usuario_aluno;

        if( !req.body.id_usuario_aluno ){
            const result = { data: '', errors: "Escolha o aluno" }
            return res.json(result);
        }

        const result = module.exports.descontaCredito(id,id_usuario_aluno);
        return res.json(result);
    },
    
    /* DESCONTA CREDITO */
    async descontaCredito(id,id_usuario_aluno){

        var db = new Database;
        
        const usuario = await db.query( ` SELECT aula.*, usuario.saldo
                                            FROM   usuario
                                            join   aula on (aula.id_usuario_aluno = usuario.id)
                                            where  usuario.id = ${id_usuario_aluno}
                                            AND    aula.id = ${id}` );

        if( usuario[0].saldo > 0 && usuario[0].saldo >= usuario[0].qtdaula ){

            var newsaldo = usuario[0].saldo - usuario[0].qtdaula;
            
            await db.query( `  UPDATE usuario SET 
                                saldo ='${newsaldo}'
                                WHERE id=${id_usuario_aluno}` ).then( rows => rows);
            
            
            const resp2 = await db.query( `  INSERT into lancamento 
                                            (id_usuario,data,qtdaula,id_tipolancamento) 
                                            VALUES ('${id_usuario_aluno}',now(),${usuario[0].qtdaula},4)` ).then( rows => rows );

            if(resp2.affectedRows > 0){

                return { data: "", errors: "" }
            } else {

                /* ENVIAR EMAIL SUPORTE */
                return { data: "", errors: "Dificuldade Técnica" }
            }                           
            
        } else {

            return { data: "", errors: "Este aluno não possui crédito" }
        }
    },



    /* Função a ser chamada pelo Findteacher do React */
    async findTeacher(req,res){
        const id = parseInt(req.params.id); /* ID AULA */
        console.log('id ',id)
        if( id != ""){
            
            await loopFindteacher(req,res,id,1);
        } else {
            console.log('fudeu')
        }
    },
}

async function loopFindteacher(req,res,id,cont) {

    const id_aluno = req.body.id_aluno;
    const qtdAulas = req.body.qtdAulas;
    const totalMinutos = req.body.totalMinutos;
    const idCategoria = req.body.idCategoria;
    const displayCategoria = req.body.displayCategoria;
    const assuntos = req.body.assuntos; /* OBJETO JSON */
    const message = req.body.message;

    var db = new Database;
    var arrayProf = [];
    var loop = setInterval( async function (){ 

        console.log("Findteacher tentativa ",cont);

        const aulas = await db.query( ` SELECT aula.id ,aula.mensagem,cat.nome as displayCategoria, 
                                        us2.avatar, us2.id as id_usuario_aluno, us2.nome as nomeAluno,
                                        aula.id_usuario_professor, aula.channel, us.avatar as avatarProfessor,
                                        us.nome as nomeProfessor, aula.qtdaula 
                                        FROM aula
                                        left join usuario us on (us.id = aula.id_usuario_professor)
                                        join usuario us2 on (us2.id = aula.id_usuario_aluno)
                                        join aulaassunto aa on (aula.id = aa.id_aula)
                                        join assunto ass on (ass.id = aa.id_assunto)
                                        join subcategoria sub on (sub.id = ass.id_subcategoria)
                                        join categoria cat on (sub.id_categoria = cat.id)
                                        where  aula.id = ${id}` );

        if( aulas[0].id_usuario_professor == 0 ){
            
            console.log("Já achou Professor? Não... ",cont);

            
            const ass = await db.query( `  SELECT assunto.id, assunto.nome,aa.id_aula
                                        FROM   aulaassunto aa
                                        join   assunto on (assunto.id = aa.id_assunto)
                                        where  aa.id_aula = ${id}
                                        order by aa.id asc` );

            Object.assign(aulas[0], { assuntos: ass, origem: 'findteacher' });

            const {tokenFcm, id_professor} = await FindTeacher(aulas[0], arrayProf);//procurar professor na lista de espera            
            arrayProf.push(id_professor);
            /* {ENVIAR PUSH FIREBASE AQUI} */
            await sendPush(aulas[0], tokenFcm);

            cont++;
            
            
        } else {
            console.log("Já achou Professor? Sim... ",cont);

            clearTimeout(loop); /* ACHOU PROFESSOR */

            const result = { data: aulas[0], errors: "" }
            return res.json(result);
        }

        if( cont == 12 ){
            console.log("Finalizou o contador: ",cont);

            clearTimeout(loop); /* ACHOU PROFESSOR */
            const result = { data: "", errors: "Nenhum professor encontrado" }
            return res.json(result);
        }

    }, 5000);
}