const Database = require('../database');

module.exports = {
    async index(req,res){

        const id = parseInt(req.params.id);

        var db = new Database;
        const resp = await db.query( `SELECT * FROM subcategoria WHERE id_categoria = ${id} and deletado is null `).then( rows => rows );

        return res.json(resp);
    },
    async SubcategoriaUsuario(req,res){        
        const id_usuario = parseInt(req.params.id_usuario);
        let array = [];
        var db = new Database;
        const resp = await db.query( `SELECT * FROM usuariosubcategoria WHERE id_usuario = ${id_usuario} `).then( rows => rows );
        resp.forEach(function(data, i){
            array.push(data.id_subcategoria);
        })
        return res.json(array);
    },
    
}