const Database = require('../database');
require('../utils/email');

var table = "usuario";

module.exports = {
    async index(req,res){

        const id = parseInt(req.params.id) || 0;

        var db = new Database;
        const resp = await db.query( `SELECT   ${table}.*, 
                                                DATE_FORMAT(dt_nascimento,'%d/%m/%Y') as dt_nasc, 
                                                concat(bank.codigo,' - ',bank.nome) as displayBanco
                                        FROM   ${table} 
                                        left join bank on (bank.id = ${table}.id_banco)
                                        where  ${table}.id = ${id} `).then( rows => rows );

        
        const sub = await db.query( `  SELECT sub.*, cat.nome as categoria, cat.id as idcat
                                        FROM   usuariosubcategoria us
                                        join   subcategoria sub on (sub.id = us.id_subcategoria)
                                        join   categoria cat on (cat.id = sub.id_categoria)
                                        where  us.id_usuario = ${id}` );
        
        const data = { usuario: resp[0], subcategorias: sub, errors: "" }

        return res.json({ data,errors: ""});
    },
    async ativacao(req,res){
        const url = require('url');
        const cod = parseInt(req.params.cod) || 0;

        if( cod != 0 ){

            var db = new Database;

            const user = await db.query( `  SELECT *
                                            FROM   usuario
                                            where  codativacao = ${cod}` );

            if( user[0] !== undefined ){

                await db.query( `   UPDATE ${table} SET
                                    confirmouemail =  1
                                    where id = ${user[0].id} ` );
                //console.log('http://67.205.183.161:5000/public/templates/ativacao.html')
                
                res.redirect('http://67.205.183.161:5000/public/templates/ativacao.html');
            } else {
                res.redirect('http://67.205.183.161:5000/public/templates/error.html');
            }
        }
    },
    async cadastro(req,res){
        
        const nome  = req.body.nome;
        const email = req.body.email;        
        const login = req.body.login;
        const senha = req.body.senha;        
        const id_tipousuario  = req.body.id_tipousuario;    
    
        if( !nome||!email||!login||!senha||!id_tipousuario){
            const result = { data: "", errors: "Preencha todos os campos" }
            return res.json(result);
        }

        var db = new Database;

        /* Existe alguem já usando o login ou senha?  */
        const usuarios = await db.query( `SELECT count(1) as qtd FROM ${table} where login = '${login}' OR email = '${email}'` );


        if( usuarios[0].qtd==0 ){

            var codativacao = Math.round(new Date().getTime()/1000)

            var crypto = require('crypto');
            newpassMd5 = crypto.createHash('md5').update( senha.toString() ).digest("hex");

            const resp = await db.query( ` INSERT INTO ${table} 
                                           (nome,email,login,senha,id_tipousuario,codativacao) 
                                           VALUES
                                           ('${nome}','${email}','${login}','${newpassMd5}','${id_tipousuario}','${codativacao}')` ).then( rows => rows );

            
            /* ENVIANDO E-MAIL DE ATIVAÇÃO */
            try{

                const fs = require('fs');
                var contents = fs.readFileSync(`./public/templates/cadastro.html`).toString();
        
                contents = contents.split("{{NOME}}").join(nome);
                contents = contents.split("{{LINK}}").join(`http://67.205.183.161:5000/ativacao/${codativacao}`);
        
                sendMail({
                    email: email,
                    subject: 'Bem vindo ao Studylive! ',
                    template: contents
                });

            } catch (e) {
                console.log(e)
            }

            const result = { data: "", errors: "" }
            return res.json(result);

        } else {
            db.close();
            const result = { data: "", errors: "Login ou e-mail já usados" }
            return res.json(result);
        }
    },
    
    async esquecisenha(req,res){
        
        const email = req.params.email;        
        
        if( !email ){
            const result = { data: "", errors: "Preencha todos os campos" }
            return res.json(result);
        }

        var db = new Database;

        /* Existe alguem já usando o login ou senha?  */
        const usuarios = await db.query( `SELECT * FROM ${table} where email = '${email}'` );


        if( usuarios[0] !== undefined ){

            /* ENVIANDO E-MAIL DE ATIVAÇÃO */
            try{

                var newpass = Math.round(Math.random() * (9999 - 1111) + 1111);

                var crypto = require('crypto');
                newpassMd5 = crypto.createHash('md5').update( newpass.toString() ).digest("hex");

                await db.query( ` UPDATE ${table} SET
                                            senha =  '${newpassMd5}'
                                            where id = ${usuarios[0].id} ` )

                const fs = require('fs');
                var contents = fs.readFileSync(`./public/templates/esquecisenha.html`).toString();
        
                contents = contents.split("{{NOME}}").join(usuarios[0].nome);
                contents = contents.split("{{LOGIN}}").join(usuarios[0].login);
                contents = contents.split("{{SENHA}}").join(newpass);
        
                sendMail({
                    email: email,
                    subject: 'Recuperação de Senha ',
                    template: contents
                });

            } catch (e) {
                console.log(e)
            }

            const result = { data: "", errors: "" }
            return res.json(result);

        } else {
            db.close();
            const result = { data: "", errors: "E-mail não encontrado" }
            return res.json(result);
        }
    },
    async update(req,res){

        var moment = require('moment');
        moment.suppressDeprecationWarnings = true;
        
        const id  = req.body.id || '';
        const apelido  = req.body.apelido || '';
        const nome  = req.body.nome || '';
        const login = req.body.login || '';
        const email = req.body.email || '';  
        const cpf = req.body.cpf || '';  
        const rg = req.body.rg || '';  
        const dt_nascimento = moment(req.body.dt_nascimento, "DD/MM/YYYY").format("YYYY-MM-DD") || '';  
        const celular = req.body.celular || '';  
        const uf = req.body.uf || '';  
        const cep = req.body.cep || '';  
        const logradouro = req.body.logradouro || '';  
        const numero = req.body.numero || '';  
        const complemento = req.body.complemento || '';  
        const bairro = req.body.bairro || '';  
        const cidade = req.body.cidade || '';  
        const id_banco = req.body.id_banco || 0;  
        const agencia = req.body.agencia || '';  
        const tipoconta = req.body.tipoconta || 0;  
        const contacorrente = req.body.contacorrente || '';  
        const poupanca = req.body.poupanca || '';  
        const variacaopoupanca = req.body.variacaopoupanca || '';  
        const subcat = req.body.selectedBoxes || '';  

        if( !nome||!email||!login){
            const result = { data: "", errors: "Preencha todos os campos" }
            return res.json(result);
        }

        var db = new Database;

        const usuarios = await db.query( `UPDATE ${table} set  
                                            apelido = '${apelido}',
                                            nome = '${nome}',
                                            login = '${login}',
                                            cpf = '${cpf}',
                                            rg = '${rg}',
                                            email = '${email}',
                                            celular = '${celular}',
                                            dt_nascimento = '${dt_nascimento}',
                                            uf = '${uf}',
                                            cep = '${cep}',
                                            logradouro = '${logradouro}',
                                            numero = '${numero}',
                                            complemento = '${complemento}',
                                            bairro = '${bairro}',
                                            cidade = '${cidade}',
                                            id_banco = '${id_banco}',
                                            agencia = '${agencia}',
                                            tipoconta = '${tipoconta}',
                                            contacorrente = '${contacorrente}',
                                            poupanca = '${poupanca}',
                                            variacaopoupanca = '${variacaopoupanca}',
                                            configurouconta = 1
                                            where id = ${id}` );

        if( subcat != "" ){
            await db.query( `delete from usuariosubcategoria where id_usuario = ${id}` );

            for( var x=0; x<subcat.length;x++ ){
                await db.query( `insert into usuariosubcategoria (id_usuario,id_subcategoria) VALUES (${id},${subcat[x].id})` );
            }
        }

        return res.json(usuarios);
        
    },
    async updateToken(req,res){        
        const id = parseInt(req.params.id) || 0;
        const tokenFcm  = req.body.tokenFcm || '';
        

        if( !id||!tokenFcm){
            const result = { data: "", errors: "Preencha todos os campos" }
            return res.json(result);
        }

        var db = new Database;

        const usuarios = await db.query( `UPDATE ${table} set
                                            token_fcm = '${tokenFcm}'
                                            where id = ${id}` );

        return res.json(usuarios);
        
    },
    async login(req,res){
        
        const login = req.body.login;
        const senha = req.body.senha;        
    
        if( !login||!senha){
            const result = { data: "", errors: "Preencha todos os campos" }
            return res.json(result);
        }        
        
        var db = new Database;

        var crypto = require('crypto');
        newpassMd5 = crypto.createHash('md5').update( senha.toString() ).digest("hex");

        const usuarios = await db.query( `SELECT * FROM ${table} where login = '${login}' and senha = '${newpassMd5}'` ).then( rows => rows );
        
        if( usuarios[0] ){

            db.close();
            return res.json({data: usuarios[0],errors: ""});

        } else {
            db.close();
            const result = { data: "", errors: "Não encontrado" }
            return res.json(result);
        }
    },

    async setBgimage(req,res){
        
        try
        {
            const id  = req.params.id;    
            var db = new Database;
            var times = Math.round(new Date().getTime()/1000)

            var base64Data = req.body.data.replace(/^data:image\/jpg;base64,/, "");
            
            await db.query( `update ${table} set bgimage = '${times}' where id = ${id}` )

            require("fs").writeFile("./photos/"+id+"_bgimage"+times+".jpg", base64Data, 'base64', function(err) {
                const result = { data: id+"_bgimage"+times+".jpg", errors: "" }
                return res.json(result);
            });
    
        }
        catch(error)
        {
            console.log('ERROR:', error);
        }
    },

    async getBgimage(req,res){
        
        const id  = req.params.id;    

        var db = new Database;

        const usuarios = await db.query( `SELECT bgimage FROM ${table} where id = ${id}` );
        
        if( usuarios[0] ){

            const result = { data: usuarios[0].bgimage, errors: "" }
            return res.json(result);

        } else {
            const result = { data: "", errors: "Imagem não encontrada" }
            return res.json(result);
        }
    },
    
    async setAvatar(req,res){
        
        try
        {
            const id  = req.params.id;    
            var db = new Database;
            var times = Math.round(new Date().getTime()/1000)

            var base64Data = req.body.data.replace(/^data:image\/jpg;base64,/, "");
            
            await db.query( `update ${table} set avatar = '${times}' where id = ${id}` )

            require("fs").writeFile("./photos/"+id+"_avatar"+times+".jpg", base64Data, 'base64', function(err) {
                const result = { data: id+"_avatar"+times+".jpg", errors: "" }
                return res.json(result);
            });
    
        }
        catch(error)
        {
            console.log('ERROR:', error);
        }
    
    },
    
    async getAvatar(req,res){
        
        const id  = req.params.id;    

        var db = new Database;

        const usuarios = await db.query( `SELECT avatar FROM ${table} where id = ${id}` );
        
        if( usuarios[0] ){

            const result = { data: usuarios[0].avatar, errors: "" }
            return res.json(result);

        } else {
            const result = { data: "", errors: "Imagem não encontrada" }
            return res.json(result);
        }
    }
}