const express = require('express');
const app = express();         
const bodyParser = require('body-parser');
const port = 5000; //porta padrão
const router = require('./src/routes');
const cron = require('node-cron');
const request = require('request');
//const ipatual = 'http://192.168.0.102';
const ipatual = 'http://67.205.183.161';

app.use('/photos', express.static("./photos"));
app.use('/public', express.static("./public"));
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
app.use('/', router);

var moment = require('moment-timezone');
moment.tz.setDefault("America/Bahia");

//inicia o servidor
app.listen(port);
console.log('API funcionando!');

cron.schedule('* * * * *', () => {
    request(ipatual+':5000/suaAulaEstaProxima', function (error, response, body) {
        console.log('começou! SuaAulaEstaProxima111111')
        /* CRON para chamar a Central a cada 1 minuto */
    });
    request(ipatual+':5000/entrarSalaEspera', function (error, response, body) {
        console.log('começou! entrarSalaEspera22222')
        /* CRON para chamar a Central a cada 1 minuto */
    });
});

